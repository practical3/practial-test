$(document).ready(function() 
{
	//Load the Datatable.
    $('#example').DataTable();
    
    //Get the date and disabled the future date in Date of Birth
    var currentDate = new Date();
	$('.date_of_birth').datepicker
	({
		format: 'dd/mm/yyyy',
		autoclose:true,
		endDate: "currentDate",
		maxDate: currentDate
	}).on('changeDate', function (ev) 
	{
		$(this).datepicker('hide');
	});
	$('.date_of_birth').keyup(function ()
	{
		if (this.value.match(/[^0-9]/g)) 
		{
			this.value = this.value.replace(/[^0-9^-]/g, '');
		}
	});

	//Sign form client side validation.
	$.validator.addMethod("firstnameValidation", function(value, element) 
	{
		return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
	}, "Please enter valid first name");
	$.validator.addMethod("lastnameValidation", function(value, element) 
	{
		return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
	},"Please enter valid last name");

	$.validator.addMethod('filesize', function (value, element, param) {
	    return this.optional(element) || (element.files[0].size <= param)
	}, 'File size must be less than {0}');
	$(".sign-up-form").validate
	({
		rules:
		{
			first_name: 
			{
				required			: true,
				minlength			: 1,
				maxlength			: 20,
				firstnameValidation	:true
				
			},
			last_name: 
			{
				required			: true,
				minlength			: 1,
				maxlength			: 20,
				lastnameValidation	:true
			},
			email: 
			{
				required	: true,
				minlength	: 1,
				maxlength	: 20,
				email		: true
			},
			password: 
			{
				required	: true,
				minlength	: 1,
				maxlength	: 20,
			},
			date_of_birth: 
			{
				required	: true
			},
			profile_image: 
			{
				required	: true,
				extension: "jpg,jpeg",
                filesize: 5242880,
			},
			roles: 
			{
				required	: true
			}
		},
		messages:
		{
			first_name: 
			{
				required: "Please enter first name.",
			},
			last_name: 
			{
				required: "Please enter last name.",
			},
			email: 
			{
				required: "Please enter email id.",
			},
			password: 
			{
				required: "Please enter password.",
			},
			date_of_birth: 
			{
				required: "Please select date of birth.",
			},
			profile_image: 
			{
				required: "Please select profile image.",
				extension : "Please select image with jpg|.jpeg"
			},
			roles: 
			{
				required: "Please select role."
			}
		},
		submitHandler: function() 
		{
			$( ".sign-up-form" ).submit();
		}
	});

	//Login form validation
	$(".login-form").validate
	({
		rules:
		{
			email: 
			{
				required	: true,
				minlength	: 1,
				maxlength	: 20,
				email		: true
			},
			password: 
			{
				required	: true,
				minlength	: 1,
				maxlength	: 20,
			}
		},
		messages:
		{
			email: 
			{
				required: "Please enter email id.",
			},
			password: 
			{
				required: "Please enter password.",
			}
		},
		submitHandler: function() 
		{
			$( ".login-form" ).submit();
		}
	});

	setTimeout(function() { $(".alert").hide(); }, 5000);
});