@extends('layouts.main')

@section('content')
	<div class="container">
	    <div class="signup-form">
		    <form action="<?php echo route('sign-up-user'); ?>" method="post" class="sign-up-form" autocomplete="off"  enctype="multipart/form-data">
		    	@csrf
		    	<h2>Sign Up</h2>
		    	<div class="form-group">
		        	<input type="text" class="form-control first_name" name="first_name" placeholder="First Name" >
		        </div>
		        <div class="form-group">
		        	<input type="text" class="form-control last_name" name="last_name" placeholder="Last Name" >
		        </div>
		        <div class="form-group">
		        	<input type="file" class="form-control profile_image"  id="profile_image" name="profile_image" accept="image/*"  placeholder="Profile Image" >
		        </div>
				
				<div class="form-group">
		        	<input type="email" class="form-control email" name="email" placeholder="Email">
		        </div>
				<div class="form-group">
		            <input type="password" class="form-control password" name="password" placeholder="Password">
		        </div>
				<div class="form-group">
		            <input type="text" class="form-control date_of_birth" name="date_of_birth" placeholder="Date Of Birth">
		        </div>
		        <div class="form-group">
		            <select name="roles" id="roles" class="form-control" placeholder="Select Roles">
						<option value="1">Admin</option>
						<option value="2">User</option>
					</select>
		        </div>        
		        <div class="form-group">
		            <button type="submit" class="btn btn-success btn-lg btn-block">Register Now</button>
		        </div>
		    </form>
		</div>
	</div>
@endsection