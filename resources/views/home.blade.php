@extends('layouts.main')

@section('content')
	<div class="container">
	    <h2>Blog Details</h2>
	    <table id="example" class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>User Name</th>
	                <th>Title</th>
	                <th>Description</th>
	                <th>Start Date</th>
	                <th>End Date</th>
	            </tr>
	        </thead>
	        <tbody>
	        	<?php 
	        		if(count($getBlogDetails) != 0)
	        		{
	        			foreach ($getBlogDetails as $getBlogDetail) 
	        			{	        			
	        	?>
	        			<tr>
			                <td>{{$getBlogDetail['user_id']}}</td>
			                <td>{{$getBlogDetail['title']}}</td>
			                <td>{{$getBlogDetail['description']}}</td>
			                <td>{{$getBlogDetail['start_date']}}</td>
			                <td>{{$getBlogDetail['end_date']}}</td>
			            </tr>
	        	<?php		
	        			}
	        		}
	        	?>
	        </tbody>
	    </table>
	</div>
@endsection