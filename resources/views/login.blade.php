@extends('layouts.main')

@section('content')
	<div class="container">
	    <div class="signup-form">
		    <form action="<?php echo route('login-user'); ?>" method="post" class="login-form" autocomplete="off">
		    	@csrf
		    	<h2>Login</h2>
		    	<div class="form-group">
		        	<input type="email" class="form-control email" name="email" placeholder="Email">
		        </div>
				<div class="form-group">
		            <input type="password" class="form-control password" name="password" placeholder="Password">
		        </div>
				<div class="form-group">
		            <button type="submit" class="btn btn-success btn-lg btn-block">Login</button>
		        </div>
		    </form>
		</div>
	</div>
@endsection