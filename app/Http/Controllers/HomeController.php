<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Console\Presets\Boostrap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Blog;
use App\User;

class HomeController extends Controller
{
	/**
	*Default Method
	*/
	public function __construct()
	{

	}
	/**
	* Home Page Method. 
	* Get the Blog information from database.
	*/
	public function index()
	{
		//Set page title.
		$title 			= "Home";
		//Get the blog information from database table.
		$getBlogDetails = Blog::where('is_active',1)->get()->toArray();
		return view('home',compact('title','getBlogDetails'));		
	}
	/**
	* Sign Up view page Method. 
	* Load the Sign Up form view.
	*/
	public function signUpView()
	{
		//Set page title.
		$title 			= "Sign Up";
		return view('sign-up',compact('title'));		
	}

	/**
	* Sign the user or Admin
	*/
	public function signUpUser(Request $request)
	{
		$validated = $request->validate
		([
	        'first_name'	=> 'required|min:1|max:20',
	        'last_name' 	=> 'required|min:1|max:20',
	        'email' 		=> 'required|min:1|max:20|email',
	        'password' 		=> 'required|min:1|max:20',
	        'date_of_birth' => 'required',
	        'profile_image' => 'required',
	        'roles' 		=> 'required',
	    ]);
		if(!$validated)
		{
			flash('Something went wrong');
			return back();
		}
		else
		{
			$image 		= $request['profile_image'];
			$imageName 	= time().'.'.$request->profile_image->extension();  
			$request->profile_image->move(public_path('images'), $imageName);
			$userInfo 	= User::Create
			([
				'first_name' 	=> trim($request['first_name']),
				'last_name' 	=> trim($request['last_name']),
				'email'	 		=> trim($request['email']),
				'password' 		=> bcrypt(trim($request['password'])),
				'dob' 			=> date('Y-m-d',strtotime(trim($request['date_of_birth']))),
				'image' 		=> trim($imageName),
				'role' 			=> trim($request['roles']),
			]);
			if(!$userInfo)
			{
				flash('Something went wrong');
				return back();
			}	
			return redirect('/login');
		}
	}

	/**
	* Blog view
	*/
	public function blog()
	{
		
	}
}
