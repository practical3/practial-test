<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Provider\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
	protected $redirectTo = '/home';

	use AuthenticatesUsers;
	/**
	*Default Method
	*/
	public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}	

	public function index()
	{
		//Set page title.
		$title 			= "Login";
		return view('login',compact('title'));	
	}
}

